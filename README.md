TurtleTB
========

I made this program for a friend. I figured it was good enough to release.

It is in the public domain, so do whatever you want. I mostly don't care, though please credit me if you modify it, (though I can't prevent you from distributing this and not crediting me) or I'll be sad :(
